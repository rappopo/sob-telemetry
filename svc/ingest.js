const { _, Sequelize } = require('rappopo-sob').Helper

const schema = {
  mid: Sequelize.STRING(20),
  source: Sequelize.STRING(20),
  account: Sequelize.STRING(20),
  msgType: Sequelize.STRING(20),
  payload: Sequelize.TEXT
}

const fields = _.concat(
  ['id', 'createdAt'],
  _.keys(schema)
)

const entityValidator = {
  mid: { type: 'string', empty: false, trim: true },
  source: { type: 'string', empty: false, trim: true },
  account: { type: 'string', empty: false, trim: true },
  msgType: { type: 'string', empty: false, trim: true }
}

module.exports = ({ dbSql }) => {
  return {
    mixins: [dbSql],
    model: {
      define: schema,
      options: {
        updatedAt: false,
        underscored: true,
        indexes: [
          { fields: ['mid'] },
          { fields: ['source'] },
          { fields: ['account'] },
          { fields: ['msg_type'] },
          { fields: ['created_at'] }
        ]
      }
    },
    settings: {
      broadcastRecord: true,
      entityValidator,
      webApi: { disabled: ['update', 'remove'] },
      fields
    },
    hooks: {
      before: {
        create: ['beforeCreate'],
        apiCreate: ['beforeCreate']
      }
    },
    methods: {
      async beforeCreate (ctx) {
        const keys = ['mid', 'source', 'account', 'msgType', 'token']
        const required = _.pick(ctx.params, keys)
        const payload = {}
        _.forOwn(ctx.params, (v, k) => {
          if (keys.includes(k)) return
          const f = parseFloat(v)
          payload[k] = _.isNaN(f) ? v : f
        })
        ctx.params = _.merge(required, { payload: JSON.stringify(payload) })
      }
    }
  }
}
